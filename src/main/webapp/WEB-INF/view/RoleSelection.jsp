<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>RoleSelection</title>
    <%--    <link rel="stylesheet" href="css/PokemonList2.css">--%>
</head>

<body>
    <header>
        <div class="title">
            Выбирете роль.
<%--            Роль "Лох" позволяет только просматривать маленьких покемонов.--%>
<%--            Роль "Бох" дает возможность просматрвать больших пакемонов.--%>
        </div>
    </header>

    <form method="post">
        <div class="button">
            <input type="submit" name="PokemonListBokh" value="Бох"/>
            <input type="submit" name="PokemonListLokh" value="Лох"/>
        </div>
    </form>

</body>
</html>