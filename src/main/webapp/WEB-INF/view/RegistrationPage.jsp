<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>RegistrationPage</title>
    <link rel="stylesheet" href="css/RegistrationPage.css">
</head>
<body>

<section class="container">
    <div class="login">
        <h1>Регистрация пользователя</h1>
        <form method="post" >  <%--action="index.html"--%>
            <p><input type="text" name="login" value="" placeholder="Логин"></p>
            <p><input type="password" name="password" value="" placeholder="Пароль"></p>
            <p class="submit"><input type="submit" name="commit" value="Ok"></p>
        </form>
    </div>

</section>
</body>
</html>
