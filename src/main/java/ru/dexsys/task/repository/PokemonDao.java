package ru.dexsys.task.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.dexsys.task.resources.entity.Pokemon;

@Repository
public interface PokemonDao extends CrudRepository<Pokemon, Integer> {
}
