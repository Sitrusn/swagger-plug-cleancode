package ru.dexsys.task.repository;

import org.springframework.data.repository.CrudRepository;
import ru.dexsys.task.resources.entity.User;

public interface UserDao extends CrudRepository<User, Integer> {
    User findByName(String name);
}