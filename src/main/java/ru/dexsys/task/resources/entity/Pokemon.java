package ru.dexsys.task.resources.entity;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;

@Entity
@Table(name = "pokemons", schema = "public")
public class Pokemon extends ResourceSupport{

    @Id//Переменную id заменил на pokemonId - для hateoas'a.
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int pokemonId;

    @Column(name = "name")
    private String name;

    @Column(name = "url")
    private String url;

    public Pokemon() {
    }

    public int getPokemonId() {
        return this.pokemonId;
    }

    public String getName() {
        return this.name;
    }

    public String getUrl() {
        return this.url;
    }

    public void setPokemonId(int pokemonId) {
        this.pokemonId = pokemonId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Pokemon)) return false;
        final Pokemon other = (Pokemon) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getPokemonId() != other.getPokemonId()) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$url = this.getUrl();
        final Object other$url = other.getUrl();
        if (this$url == null ? other$url != null : !this$url.equals(other$url)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Pokemon;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * PRIME + this.getPokemonId();
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $url = this.getUrl();
        result = result * PRIME + ($url == null ? 43 : $url.hashCode());
        return result;
    }

    public String toString() {
        return "Pokemon(pokemonId=" + this.getPokemonId() + ", name=" + this.getName() + ", url=" + this.getUrl() + ")";
    }
}