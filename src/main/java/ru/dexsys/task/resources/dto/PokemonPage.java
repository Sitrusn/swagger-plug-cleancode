package ru.dexsys.task.resources.dto;

import lombok.Data;
import ru.dexsys.task.resources.entity.Pokemon;
import java.util.List;

@Data
public class PokemonPage {
    private int count;
    private String next;
    private String previous;
    private List<Pokemon> results;
}
