package ru.dexsys.task.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.dexsys.task.resources.dto.PokemonPage;

@Component
public class PokemonGateway {

    @Autowired
    private RestTemplate restTemplate;

    public ResponseEntity<PokemonPage> getPokemonList(int offset) {
        return restTemplate.exchange("https://pokeapi.co/api/v2/pokemon/?limit=20&offset=" + offset, HttpMethod.GET, getDefaultEntity(), PokemonPage.class);
    }

    private HttpEntity getDefaultEntity() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("User-Agent", "Google Chrome");
        return new HttpEntity<>(httpHeaders);
    }

}