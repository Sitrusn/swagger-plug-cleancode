package ru.dexsys.task.plug;

import org.springframework.stereotype.Component;
import ru.dexsys.task.resources.entity.Pokemon;

import java.util.ArrayList;
import java.util.List;

@Component
public class PokemonListPlug {

    private List<Pokemon> pokemonList;
    private Pokemon testPokemon;

    public List<Pokemon> getTestPokemonList() {

        pokemonList = new ArrayList<Pokemon>();
        testPokemon = new Pokemon();

        testPokemon.setPokemonId(999);
        testPokemon.setName("testPokemon");
        testPokemon.setUrl("http://localhost:8080/testPokemon");

        for (int i = 0; i < 20; i++) {
            pokemonList.add(testPokemon);
        }

        return pokemonList;
    }
}
