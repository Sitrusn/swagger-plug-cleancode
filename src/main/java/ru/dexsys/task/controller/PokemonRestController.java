package ru.dexsys.task.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.dexsys.task.resources.entity.Pokemon;
import ru.dexsys.task.service.*;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/pokemonList")
@AllArgsConstructor
public class PokemonRestController {

    @Autowired
    private PokemonService pokemonService;

    @Autowired
    private PageNavigation pageNavigation;

    @GetMapping
    public ModelAndView getPokemonList() {
        List<Pokemon> pokemonList = pokemonService.getPokemonList(20);
        return new ModelAndView(pageNavigation.getUserRole(), "PokemonList", pokemonList);
    }

    @PostMapping()
    public ModelAndView buttonIsPressed(HttpServletRequest request) throws NullPointerException{
        int offset;
        switch (request.getParameter("button")) {
            case ("Next"):
                offset = pageNavigation.getNextOffset();
                break;
            case ("Previous"):
                offset = pageNavigation.getPreviousOffset();
                break;
            default:
                throw new NullPointerException("Offset variable is not set!");
        }
        return new ModelAndView(pageNavigation.getUserRole(), "PokemonList", pokemonService.getPokemonList(offset));
    }

}