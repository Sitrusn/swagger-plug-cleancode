package ru.dexsys.task.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/role")
@SessionAttributes()
public class UserRoleController {

    @GetMapping
    public ModelAndView getRoleSelectionPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("RoleSelection");
        return modelAndView;
    }

    @PostMapping()
    public String setUserRole(HttpServletRequest request) {
        String userRole = request.getParameterNames().nextElement();
        request.getSession().setAttribute("userRole", userRole);
        return "redirect:" + "http://localhost:8080/pokemonList";
    }

}
