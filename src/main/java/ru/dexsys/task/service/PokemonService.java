package ru.dexsys.task.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.dexsys.task.plug.PokemonListPlug;
import ru.dexsys.task.resources.entity.Pokemon;
import ru.dexsys.task.gateway.PokemonGateway;
import java.util.List;

@Service
public class PokemonService {

    @Autowired
    private PokemonGateway pokemonGateway;

    @Autowired
    private PokemonListPlug pokemonListPlug;

    @Value("${user.togglepoint}")
    private boolean togglepoint;

    public List<Pokemon> getPokemonList(int offset) {
        if (togglepoint) {
            List<Pokemon> pokemonList = pokemonGateway.getPokemonList(offset).getBody().getResults();
            pokemonList.stream().forEach(pokemon -> pokemon.setPokemonId(getPokemonIdFromUrl(pokemon)));
            return pokemonList;
        } else {
            return pokemonListPlug.getTestPokemonList();
        }
    }

    public int getPokemonIdFromUrl(Pokemon pokemon) {
        return Integer.parseInt(pokemon.getUrl().split("/")[6]);
    }
}