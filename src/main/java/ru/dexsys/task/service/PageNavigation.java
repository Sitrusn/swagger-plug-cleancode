package ru.dexsys.task.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.dexsys.task.resources.entity.Pokemon;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class PageNavigation {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private PokemonService pokemonService;

    public int getCurrentWebPageNumber() {
        return (int) Integer.parseInt(request.getParameter("pokemonId")) / 20;
    }

    public int getPreviousOffset() {
        return 20 * (getCurrentWebPageNumber() - 1);
    }

    public int getNextOffset() {
        return 20 * (getCurrentWebPageNumber() + 1);
    }

    public String getUserRole(){
        return (String) request.getSession().getAttribute("userRole");
    }

}